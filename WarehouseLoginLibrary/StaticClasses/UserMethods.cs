﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseLoginLibrary.Models;
using WarehouseLoginLibrary.StaticClasses;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using NLog;

namespace WarehouseLoginLibrary.Methods
{
    public static class UserMethods
    {
        public static int AppID;

        public static int UserMethodsSetup(string appName)
        {
            AppID = GetAppIDByAppName(appName);

            return AppID;
        }
        public static User GetUserByUsernameAndPin(string username, string pin)
        {
            User user = new User();

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Username", username);
                parameters.Add("@Pin", pin);
                parameters.Add("@AppID", AppID);

                user = DB.Query<User>("SelectUserByUsernameAndPin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return user;
        }

        public static List<User> GetUsers()
        {
            List<User> users = new List<User>();

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@appID", AppID);

                users = DB.Query<User>("SelectAllUsersByAppID", param: parameters, commandType: CommandType.StoredProcedure).ToList();
            }

            return users;
        }

        public static int AddUser(User newUser)
        {
            int newID = 0;

            var parameters = new DynamicParameters();
            parameters.Add("@FirstName", newUser.FirstName);
            parameters.Add("@LastName", newUser.LastName);
            parameters.Add("@UserName", newUser.UserName);
            parameters.Add("@Pin", newUser.Pin);
            parameters.Add("@PageID", newUser.PageID);
            parameters.Add("@IsAdmin", newUser.IsAdmin);
            parameters.Add("@CreatedDate", newUser.CreatedDate);
            parameters.Add("@CreatedBy", newUser.CreatedBy);
            parameters.Add("@LastUpdatedDate", newUser.LastUpdatedDate);
            parameters.Add("@LastUpdatedBy", newUser.LastUpdatedBy);
            parameters.Add("@Active", 1);
            parameters.Add("@AppID", AppID);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                newID = DB.Query<int>("InsertUser", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }

            return newID;
        }

        public static int UpdateUser(User updatedUser)
        {
            int success = 0;

            var parameters = new DynamicParameters();
            parameters.Add("@UserID", updatedUser.UserID);
            parameters.Add("@FirstName", updatedUser.FirstName);
            parameters.Add("@LastName", updatedUser.LastName);
            parameters.Add("@UserName", updatedUser.UserName);
            parameters.Add("@Pin", updatedUser.Pin);
            parameters.Add("@PageID", updatedUser.PageID);
            parameters.Add("@IsAdmin", updatedUser.IsAdmin);
            parameters.Add("@CreatedDate", updatedUser.CreatedDate);
            parameters.Add("@CreatedBy", updatedUser.CreatedBy);
            parameters.Add("@LastUpdatedDate", updatedUser.LastUpdatedDate);
            parameters.Add("@LastUpdatedBy", updatedUser.LastUpdatedBy);
            parameters.Add("@Active", updatedUser.Active);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                DB.Execute("UpdateUser", parameters, commandType: CommandType.StoredProcedure);
                success = 1;
            }

            return success;
        }

        public static User GetUserByID(int userID)
        {
            User user = new User();

            var parameters = new DynamicParameters();
            parameters.Add("@UserID", userID);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                user = DB.Query<User>("SelectUserByID", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }

            if (user == null)
            {
                user = new User();
            }

            return user;
        }

        public static List<Page> GetPages()
        {
            List<Page> pages = new List<Page>();

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@AppID", AppID);

                    pages = DB.Query<Page>("SelectAllPagesByAppID", param: parameters, commandType: CommandType.StoredProcedure)
                        .ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return pages;
        }

        public static Page GetPageByID(int pageID)
        {
            Page page = new Page();

            var parameters = new DynamicParameters();
            parameters.Add("@PageID", pageID);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                page = DB.Query<Page>("SelectPageByID", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }

            return page;
        }

        public static Page GetPageByName(string pageName)
        {
            Page page = new Page();

            var parameters = new DynamicParameters();
            parameters.Add("@PageName", pageName);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                page = DB.Query<Page>("SelectPageByName", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }

            return page;
        }

        public static bool GetPageAccessByUserIDAndPageID(int userID, int pageID)
        {
            bool access = false;

            var parameters = new DynamicParameters();
            parameters.Add("@UserID", userID);
            parameters.Add("@PageID", pageID);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                access = DB.Query<bool>("SelectAccessByUserIDAndPageID", parameters,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return access;
        }

        public static bool GetPageAccessByUserIDAndPageLocation(int userID, string pageLocation)
        {
            bool access = false;

            var parameters = new DynamicParameters();
            parameters.Add("@UserID", userID);
            parameters.Add("@PageLocation", pageLocation);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                access = DB.Query<bool>("SelectAccessByUserIDAndPageLocation", parameters,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return access;
        }

        public static int GetMenuFromPageLocation(string pageLocation)
        {
            int menuID = 0;

            var parameters = new DynamicParameters();
            parameters.Add("@PageLocation", pageLocation);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                menuID = DB.Query<int>("SelectMenuIDByPageLocation", parameters,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return menuID;
        }

        public static string GetMenuNameFromPageLocation(string pageLocation)
        {
            string menuName = "";

            var parameters = new DynamicParameters();
            parameters.Add("@PageLocation", pageLocation);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                menuName = DB.Query<string>("SelectMenuNameByPageLocation", parameters,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return menuName;
        }

        public static bool UpdateUserAccess(int userID, int pageID)
        {
            bool success = false;

            var parameters = new DynamicParameters();
            parameters.Add("@UserID", userID);
            parameters.Add("@PageID", pageID);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                DB.Query<bool>("UpdateUserAccess", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
                success = true;
            }

            return success;
        }

        public static bool DeleteUserAccess(int userID)
        {
            bool success = false;

            var parameters = new DynamicParameters();
            parameters.Add("@UserID", userID);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                DB.Query<bool>("DeleteUserAccess", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
                success = true;
            }

            return success;
        }

        public static int GetAppIDByAppName(string appName)
        {
            int appID = 0;

            var parameters = new DynamicParameters();
            parameters.Add("@AppName", appName);

            using (IDbConnection DB = DBConnection.CreateConnection())
            {
                appID = DB.Query<int>("SelectAppIDByAppName", parameters, commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }

            return appID;
        }


    }
}

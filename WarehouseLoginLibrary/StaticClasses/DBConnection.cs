﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace WarehouseLoginLibrary.StaticClasses
{
    public static class DBConnection
    {
        public static string NFSShopFloorConnectionString = "";

        public static IDbConnection CreateConnection()
        {
            if (string.IsNullOrEmpty(NFSShopFloorConnectionString))
                throw new Exception("The NFS Shop Floor Connection string is not set. Please correct.");

            return new SqlConnection(NFSShopFloorConnectionString);
        }

        public static void Setup(bool DeveloperMode)
        {
            if (!DeveloperMode)
            {
                NFSShopFloorConnectionString = WarehouseLoginLibrary.Properties.Settings.Default.WarehouseLoginLive;
            }
            else
            {
                NFSShopFloorConnectionString = WarehouseLoginLibrary.Properties.Settings.Default.WarehouseLoginDev;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WarehouseLoginLibrary.Models;

namespace WarehouseLoginLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IWarehouseLoginService
    {
        [OperationContract]
        int Setup(bool DeveloperMode = false, string AppName = "");

        [OperationContract]
        User GetUserByUsernameAndPin(string username, string pin);

        [OperationContract]
        List<User> GetUsers();

        [OperationContract]
        int AddUser(User newUser);

        [OperationContract]
        int UpdateUser(User updatedUser);

        [OperationContract]
        User GetUserByID(int userID);

        [OperationContract]
        List<Page> GetPages();

        [OperationContract]
        Page GetPageByID(int pageID);

        [OperationContract]
        Page GetPageByName(string pageName);

        [OperationContract]
        bool GetPageAccessByUserIDAndPageID(int userID, int pageID);

        [OperationContract]
        bool GetPageAccessByUserIDAndPageLocation(int userID, string pageLocation);

        [OperationContract]
        int GetMenuFromPageLocation(string pageLocation);

        [OperationContract]
        string GetMenuNameFromPageLocation(string pageLocation);

        [OperationContract]
        bool UpdateUserAccess(int userID, int pageID);

        [OperationContract]
        bool DeleteUserAccess(int userID);

        [OperationContract]
        int GetAppIDByAppName(string appName);


        // TODO: Add your service operations here
    }
}

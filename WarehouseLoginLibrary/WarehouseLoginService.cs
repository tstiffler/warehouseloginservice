﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WarehouseLoginLibrary.Methods;
using WarehouseLoginLibrary.Models;
using WarehouseLoginLibrary.StaticClasses;
using NLog;

namespace WarehouseLoginLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class WarehouseLoginService : IWarehouseLoginService
    {
        public WarehouseLoginService()
        {
        }

        public int Setup(bool developerMode = true, string appName = "")
        {
            int appID = 0;

            try
            {
                DBConnection.Setup(developerMode);
                appID = UserMethods.UserMethodsSetup(appName);
            }

            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return appID;
        }

        #region Logins

        public User GetUserByUsernameAndPin(string username, string pin)
        {
            User user = new User();

            try
            {
                user = UserMethods.GetUserByUsernameAndPin(username, pin);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return user;
        }

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();

            try
            {
                users = UserMethods.GetUsers();
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return users;
        }

        public int AddUser(User newUser)
        {
            int newID = 0;

            try
            {
                newID = UserMethods.AddUser(newUser);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return newID;
        }

        public int UpdateUser(User updatedUser)
        {
            int success = 0;

            try
            {
                success = UserMethods.UpdateUser(updatedUser);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return success;
        }

        public User GetUserByID(int userID)
        {
            User user = new User();

            try
            {
                user = UserMethods.GetUserByID(userID);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return user;
        }

        #endregion Logins   

        #region Pages

        public List<Page> GetPages()
        {
            List<Page> pages = new List<Page>();

            try
            {
                pages = UserMethods.GetPages();
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return pages;
        }

        public Page GetPageByID(int pageID)
        {
            Page page = new Page();

            try
            {
                page = UserMethods.GetPageByID(pageID);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return page;
        }

        public Page GetPageByName(string pageName)
        {
            Page page = new Page();

            try
            {
                page = UserMethods.GetPageByName(pageName);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return page;
        }

        public bool GetPageAccessByUserIDAndPageID(int userID, int pageID)
        {
            bool access = false;

            try
            {
                access = UserMethods.GetPageAccessByUserIDAndPageID(userID, pageID);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return access;
        }

        public bool GetPageAccessByUserIDAndPageLocation(int userID, string pageLocation)
        {
            bool access = false;

            try
            {
                access = UserMethods.GetPageAccessByUserIDAndPageLocation(userID, pageLocation);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return access;
        }

        public int GetMenuFromPageLocation(string pageLocation)
        {
            int menuID = 0;

            try
            {
                menuID = UserMethods.GetMenuFromPageLocation(pageLocation);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return menuID;
        }

        public string GetMenuNameFromPageLocation(string pageLocation)
        {
            string menuName = "";

            try
            {
                menuName = UserMethods.GetMenuNameFromPageLocation(pageLocation);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return menuName;
        }

        public bool UpdateUserAccess(int userID, int pageID)
        {
            bool success = false;

            try
            {
                success = UserMethods.UpdateUserAccess(userID, pageID);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return success;
        }

        public bool DeleteUserAccess(int userID)
        {
            bool success = false;

            try
            {
                success = UserMethods.DeleteUserAccess(userID);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return success;
        }

        #endregion Pages

        #region App

        public int GetAppIDByAppName(string appName)
        {
            int appID = 0;

            try
            {
                appID = UserMethods.GetAppIDByAppName(appName);
            }
            catch (Exception ex)
            {
                Logging.ErrorLogging.LogErrors(ex);
            }

            return appID;
        }

        #endregion App


    }
}

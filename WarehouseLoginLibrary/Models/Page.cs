﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseLoginLibrary.Models
{
    public class Page
    {
        public int PageID { get; set; }
        public string PageType { get; set; }
        public string PageName { get; set; }
        public string PageLocation { get; set; }
        public int MenuID { get; set; }
    }
}

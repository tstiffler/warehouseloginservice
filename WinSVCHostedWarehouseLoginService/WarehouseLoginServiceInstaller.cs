﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace WarehouseLoginService
{
    [RunInstaller(true)]
    public partial class WarehouseLoginServiceInstaller : System.Configuration.Install.Installer
    {
        public WarehouseLoginServiceInstaller()
        {
            //InitializeComponent();
            serviceProcessInstaller1 = new ServiceProcessInstaller();
            serviceProcessInstaller1.Account = ServiceAccount.LocalSystem;
            serviceInstaller1 = new ServiceInstaller();
            serviceInstaller1.ServiceName = "WarehouseLoginService";
            serviceInstaller1.DisplayName = "WarehouseLoginService";
            serviceInstaller1.Description = "WCF Warehouse Login Service Hosted by Windows NT Service";
            serviceInstaller1.StartType = ServiceStartMode.Automatic;
            Installers.Add(serviceProcessInstaller1);
            Installers.Add(serviceInstaller1);
        }
    }
}
